[("shotgun" . "blasts")
 ("ICBM silo" . "nukes")
 ("water gun" . "drenches")
 ("flame thrower" . "roasts")
 ("tranquilizer gun" . "knocks out")
 ("blunderbuss" . "hits everything but")
 ("Mitsubishi A6M Zero" . "breaks divine wind on")
 ("Windows XP botnet" . "DDoSes")
 ("taser" . "electrocutes")
 ("Colt Peacemaker" . "pacifies")
 ("RFC1149-compliant avian carrier" . "UDP floods")
 ("Batarang" . "knocks %s down")
 ("shell" . "fork bombs")
 ("lightning bolt" . "sends %s to tartarus")
 ("Aperture Science Emergency Intelligence Incinerator" . "euthanizes")
 ("cat launcher" . "unleashes a clawnado upon")
 ("fish catapult" . "buries %s in flounder")
 ("ld50" . "ODs")
 ("pixellated sword" . "whacks %s repeatedly")
 ("silenced pistol" . "grabs and double taps")
 ("hammer" . "thors")
 ("mom" . "grounds")
 ("broken bottle" . "mutilates")
 ("IRC bot" . "flames")
 ("four horses" . "quarters")
 ("sharks with frickin' laser beams attached to their heads" . "fails to kill %s")
 ("railgun" . "frags")
 ("snake launcher" . "boa constricts")
 ("ritual sacrifice kit" . "sacrifices %s to some pagan god with a beard")
 ("rolled-up newspaper" . "whacks %s on the nose")
 ("flame thrower" . "kills %s with fire")
 ("lynch mob" . "lynches")
 ("anti-tank gun" . "obliterates")
 ("zombie horde" . "rips %s limb from limb")
 ("philosopher" . "gives %s an existential crisis")
 ("captcha" . "questions %s's humanity")
 ("crossbow" . "shoots an apple off %s's head")
 ("gatling gun" . "guns down")
 ("qi" . "hadoukens")
 ("fire flower" . "shoots fireballs at")
 ("banana peel" . "trips up")
 ("fanfic" . "burns %s's eyes out")
 ("pedant" . "bores %s to death")
 ("slamhound" . "hunts down and blows up")
 (".44 magnum" . "blows %s's head clean off")
 ("swarm of clueless noobs" . "plagues %s with silly questions")
 ("IED packed with cutlery" . "fork bombs")
 ("voodoo doll" . "remotely tortures")
 ("witch doctor" . "shrinks %s's head")
 ("railgun" . "pod-kills")
 ("salty ad hominem" . "seasons")
 ("earworm" . "infiltrates %s's auditory center")
 ("blackboard eraser" . "lobs it at")
 ("Spanish inquisition" . "gives %s 30 days notice for their heresy trial")
 ("trebuchet" . "flings a dead horse at")
 ("oil money" . "buries %s with pseudoscience")
 ("baseball bat" . "hits %s out of the park")
 ("banana" . "bashes")
 ("recycling plant" . "recycles")
 ("CRT monitor" . "slowly gives %s radiation poisoning")
 ("warehouse full of IBM XTs equipped with 300 baud modems" . "MS-DDoSes")
 ("bottle of water" . "sprays")
 ("holy hand grenade of Antioch" . "counts to five, no three, and then throws at")
 ("sledgehammer" . "knocks down %s's load-bearing limbs")
 ("microscopic flesh-eating hamster" . "hides it somewhere in %s's favourite sweater")
 ("experimental cocktail of drugs" . "injects into")
 ("whip" . "flogs")
 ("UML Diagramming Tool" . "specifies the heck out of %s")
 ("DOSEMU botnet" . "FreeDDOSes")
 ("wooden shoe" . "throws it into %s's gears")
 ("magician" . "saws %s in half")
 ("cable TV" . "subjects %s to hours of Disney sitcoms")
 ("oven" . "bakes %s into a cake")
 ("stake" . "burns")
 ("guillotine" . "beheads")
 ("K-pop" . "blasts")
 ("surround-sound system" . "shatters %s's skull with soundwaves")
 ("carpet" . "rolls %s up in it and dumps them off the pier")
 ("giant mirror array" . "burns %s from afar")
 ("phaser" . "stuns")
 ("12 pack of Andrex double quilted" . "TPs %s's house")
 ("window" . "defenestrates")
 ("toll-free support line" . "puts %s on hold")
 ("school teacher" . "flunks")
 ("vorpal blade" . "snicker snacks")
 ("potato knife" . "peels")
 ("grater" . "zests")
 ("bag of porridge" . "suffocates")
 ("chainsaw" . "performs a root canal on")]
