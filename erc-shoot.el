(require 'cl)

(let ((erc-shoot-directory (file-name-directory
                            (or load-file-name
                                buffer-file-name))))
  (with-temp-buffer
    (insert-file-contents
     (concat erc-shoot-directory
             "firearms.sexp"))
    (defvar erc-firearms
      (read (current-buffer))
      "Firearms for `erc-cmd-SHOOT'.")))

;;;###autoload
(defun erc-cmd-SHOOT (&rest target)
  "Shoot TARGET with a randomly selected firearm."
  (let* ((target (reduce (lambda (x y)
                           (format "%s %s" x y))
                         target))
         (firearm (aref erc-firearms
                        (random (length erc-firearms))))
         (weapon (car firearm))
         (action (cdr firearm))
         (3rd-person-pronoun (case erc-shoot-gender
                               (neutral "their")
                               (neuter "its")
                               (male "his")
                               (female "her"))))
    (erc-send-action (erc-default-target)
                     (if (string-match "%s" action)
                         (format "loads %s %s and %s." 3rd-person-pronoun weapon (format action target))
                       (format "loads %s %s and %s %s." 3rd-person-pronoun weapon action target)))))

(defun shoot-read-bbdb-firearms (string)
  "Read the firearms list from fsbot's BBDB given as STRING."
  (kill-new (pp-to-string (map 'list 'read (read string)))))

(defvar erc-shoot-gender 'neutral
  "The gender to use when running `erc-cmd-SHOOT'.")

(provide 'erc-shoot)
